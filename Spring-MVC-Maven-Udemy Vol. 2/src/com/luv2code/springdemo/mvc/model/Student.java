package com.luv2code.springdemo.mvc.model;

import java.util.Map;

public class Student {
	
	private String name;
	private String surname;
	private String studentNativeCountry; 
	
	private String justSomeDataForTesting;
	
	private Map<String, String> countryOptions;

	
	public Student() {
		super();
		System.out.println("In no-args Student Constructor");
	}
	
	//-----------------------------------------------------------
	//-------------------- GETTERS & SETTERS --------------------
	//-----------------------------------------------------------

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getStudentNativeCountry() {
		return studentNativeCountry;
	}

	public void setStudentNativeCountry(String studentNativeCountry) {
		this.studentNativeCountry = studentNativeCountry;
	}

	public Map<String, String> getCountryOptions() {
		return countryOptions;
	}

	public void setCountryOptions(Map<String, String> countryOptions) {
		this.countryOptions = countryOptions;
	}

	public String getJustSomeDataForTesting() {
		return justSomeDataForTesting;
	}

	public void setJustSomeDataForTesting(String justSomeDataForTesting) {
		this.justSomeDataForTesting = justSomeDataForTesting;
	}

	

}
