package com.luv2code.springdemo.mvc.controllers;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.luv2code.springdemo.mvc.model.Student;

@Controller
@RequestMapping("/student") 
public class StudentController {
	
	@Value("#{countriesFromFile}")
	private Map<String, String> countriesFromFile;

	
	@RequestMapping("/showStudentForm") 
	public String showStudentForm(Model myModel) {
		Student student = new Student();
		student.setName("Miki");   					// I did it only to check if it will put to form
		student.setJustSomeDataForTesting("XXX");   // I did it because I want to keep some data in model
		System.out.println("student.tostring == " + student);
		
		System.out.println(" Student name: " + student.getName() + " Student surname: " + student.getSurname() + " Student SomeDataForTesting: " + student.getJustSomeDataForTesting());
		
		myModel.addAttribute("student", student);
		
		myModel.addAttribute("theCountryOptions", countriesFromFile); 
		
		return "student-registration";
	}
	

	@RequestMapping("/studentConfirmation") 
	public String showStudentConfirmation(@ModelAttribute("student") Student student) {
		

		System.out.println("student.tostring == " + student);
		System.out.println(" Student name: " + student.getName() + " Student surname: " + student.getSurname() + " Student SomeDataForTesting: " + student.getJustSomeDataForTesting());

		
		return "student-confirmation-page";
	}
	
}
